import Phaser from 'phaser'
import BootScene from './scenes/BootScene'
import HomeScene from './scenes/HomeScene'
import PlayScene from './scenes/PlayScene'


function launch(containerId) {
    return new Phaser.Game({
        type: Phaser.AUTO,
        // pixelArt: true,
        // roundPixels: true,
        width: 1080,
        height: 1920,
        parent: containerId,
        backgroundColor: "#AAD8F3",
        // backgroundImage: "./game/assets/Backgorund.svg",
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_BOTH
        },
        // physics: {
        //     default: 'arcade',
        //     arcade: {
        //         gravity: {
        //             y: 800
        //         },
        //         debug: false
        //     }
        // },
        scene: [BootScene, PlayScene, HomeScene]
    })
}

export default launch
export {
    launch
}