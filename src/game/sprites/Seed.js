export default class Seed extends Phaser.GameObjects.Sprite {
    constructor(config) {
        super(config.scene, config.x, config.y, config.key)
        this.height = 100
        this.width = 100
        this.type = 'seed'
    }

}