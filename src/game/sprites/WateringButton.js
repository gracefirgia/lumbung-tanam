export default class WateringButton extends Phaser.GameObjects.Image {
    constructor(config) {
        super(config.scene, config.x, config.y, config.key)
            // this.acceleration       = 600;
            // this.body.maxVelocity.x = 200;
            // this.body.maxVelocity.y = 500;
        this.type = 'watering-button'
        this.TIME_LEFT = config.TIME_LEFT ? config.TIME_LEFT : 10
        this.WATER_LIMIT = config.WATER_LIMIT ? config.WATER_LIMIT : 50
        this.TIME_LIMIT = this.TIME_LEFT * this.WATER_LIMIT
        this.WATER_GOAL = config.WATER_GOAL ? config.WATER_GOAL : 200
        console.log(`WATER GOAL = ${this.WATER_GOAL}`)
        this.timeLeft = this.TIME_LEFT
        this.waterLimit = this.WATER_LIMIT
        this.timeLimit = this.TIME_LIMIT
        this.waterGoal = this.WATER_GOAL
        this.waterTank = config.waterTank ? config.waterTank : 0
        this.waterFilled = config.waterFilled ? config.waterFilled : 0
        this.waterForNextLevel = config.waterForNextLevel ? config.waterForNextLevel : 0
        this.waterForLevelUp = this.waterGoal - this.waterFilled
        console.log(`${waterForLevelUp} tetes lagi untuk ke tahap selanjutnya`)
    }

    preload() {
        this.load.image('watering-can-button')
        var textCountdown = this.add.text(180, 180).setFont('32px Arial Black').setFill('#000000');
        var textWaterTank = this.add.text(100, 230).setFont('32px Arial Black').setFill('#000000');
        var textWaterForLevelUp = this.add.text(70, 30).setFont('40px Arial Black').setFill('#000000');
    }

    create() {
        const wateringButton = this.add.image((this.cameras.main.width / 2) + 400, (this.cameras.height - 200), 'watering-can-button').setInteractive({
            useHandCursor: true
        })
        let scaleWateringButton = 0.3
        wateringButton.setScale(scaleWateringButton).setScrollFactor(0)
        textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

        let wateringTimer = this.time.addEvent({
            delay: 1000,
            callback: function() {
                console.log(`time left = ${timeLeft}`)
                var seconds = String(timeLeft % 60).padStart(2, "0")
                var minutes = String(Math.floor(timeLeft / 60)).padStart(2, "0")
                var output = `${minutes}:${seconds}`
                textCountdown.setText(output)
                timeLeft--

                textWaterTank.setText(`Water Ammo = ${waterTank}`)
                if (timeLeft == 0) {
                    waterLimit--
                    waterTank++
                    timeLeft = TIME_LEFT
                }
            },
            callbackScope: this,
            repeat: timeLimit
        })

    }
}