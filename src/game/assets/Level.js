
// You can write more code here

/* START OF COMPILED CODE */

class Level extends Phaser.Scene {
	
	constructor() {
		super("Level");
	}
	
	create() {
		this.scale.lockOrientation('potrait')
		let bg = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'Lumbung Tanam_BG')
		let scaleX = this.cameras.main.width / bg.width
		let scaleY = this.cameras.main.height / bg.height
		let scale = Math.max(scaleX, scaleY)
		bg.setScale(scale).setScrollFactor(0)
		var stepTreePlant = 0
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 180, 'Lumbung Tanam Screen Plant');
		this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 200, 'Lumbung Tanam_soil');
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 240, 'Lumbung Tanam_6');
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 320, 'Lumbung Tanam_5');
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 390, 'Lumbung Tanam_4');
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 480, 'Lumbung Tanam_3');
		// this.treePlant = this.add.image((this.cameras.main.width / 2)+20, this.cameras.main.height - 580, 'Lumbung Tanam_2');
		// this.treePlant = this.add.image(this.cameras.main.width / 2, this.cameras.main.height - 700, 'Lumbung Tanam_1');
		var treePlant = this.treePlant
		let scaleTreePlant = 0.8
		this.treePlant.setScale(scaleTreePlant).setScrollFactor(0)
		// this.treePlant.setInteractive();
	
		var wateringButton = this.add.image((this.cameras.main.width / 2)+400, this.cameras.main.height - 200, 'watering-can-button').setInteractive({ useHandCursor: true });
		let scaleWateringButton = 0.3
		wateringButton.setScale(scaleWateringButton).setScrollFactor(0)
		var tweens = this.tweens
		var addObject = this.add
        var cameras = this.cameras
		var onEventButton = false
		const TIME_LEFT = 10
		const WATER_LIMIT = 50
		const TIME_LIMIT = TIME_LEFT*WATER_LIMIT
		// const WATER_GOAL = this.getRandomInt((stepTreePlant+1)*WATER_LIMIT,(stepTreePlant+1)*(WATER_LIMIT*20))
		const WATER_GOAL = 200
		console.log(`WATER_GOAL = ${WATER_GOAL}`)
		var timeLimit =  TIME_LIMIT
		var timeLeft = TIME_LEFT
		var waterLimit = WATER_LIMIT
		var waterTank = 0
		var waterFilled = 0
		var waterForNextLevel = 0
		var waterForLevelUp = WATER_GOAL - waterFilled
		console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
		var textCountdown = this.add.text(180, 180).setFont('32px Arial Black').setFill('#000000');
		var textWaterTank = this.add.text(100, 230).setFont('32px Arial Black').setFill('#000000');
		var textWaterForLevelUp = this.add.text(70, 30).setFont('40px Arial Black').setFill('#000000');
		textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

		
		
		let wateringTimer = this.time.addEvent({
            delay: 1000,
            callback: function(){
				console.log(`timeLeft = ${timeLeft}`)
				var seconds = String(timeLeft % 60).padStart(2, "0");
    			var minutes = String(Math.floor(timeLeft / 60)).padStart(2, "0");
				var output = `${minutes}:${seconds}`
				textCountdown.setText(output)
				timeLeft--

				textWaterTank.setText(`Water ammo = ${waterTank}`)
				if(timeLeft == 0){
					waterLimit--
					waterTank++
					timeLeft = TIME_LEFT
					
				}

            },
            callbackScope: this,
            repeat: timeLimit
        });

		// the energy container. A simple sprite
        let energyContainer = this.add.sprite(this.cameras.main.width / 2, (this.cameras.main.height / 2)+850, "energycontainer");
 
        // the energy bar. Another simple sprite
        let energyBar = this.add.sprite(energyContainer.x + 46, energyContainer.y, "energybar");
 
        // a copy of the energy bar to be used as a mask. Another simple sprite but...
        this.energyMask = this.add.sprite(energyBar.x, energyBar.y, "energybar");
 
        // ...it's not visible...
        this.energyMask.visible = false;
 
        // and we assign it as energyBar's mask.
        energyBar.mask = new Phaser.Display.Masks.BitmapMask(this, this.energyMask);
        
		console.log(`this.energyMask.displayWidth = ${this.energyMask.displayWidth}`)
		console.log(`this.energyMask.x = ${this.energyMask.x}`)
		const FULL_ENERGY_MASK_VALUE = this.energyMask.x
		const INIT_MASK_VALUE = 100
		const LENGTH_MASK_VALUE = 486
		this.energyMask.x = INIT_MASK_VALUE;

		var energyMask = this.energyMask
		this.input.on('gameobjectup', function (pointer, gameobject) {
			if (gameobject === wateringButton && !onEventButton && waterTank>0)
			{
				onEventButton = true;
				wateringTimer.paused = true
					let wateringCan = addObject.sprite((this.cameras.main.width / 2)+300, this.cameras.main.height - 900, 'watering_can');
					let scaleWateringCan = 0.4
					wateringCan.setScale(scaleWateringCan).setScrollFactor(0)
					tweens.add({
					targets: wateringCan,
					alpha: { from: 0, to: 1 },
					duration: 500,
					ease: 'Phaser.Easing.Linear.None',
					delay: 0,
					yoyo: true,
					hold: 1000,
					repeat: 0,
					angle: -60
					});
					let waterDrop = addObject.sprite((this.cameras.main.width / 2)+180, this.cameras.main.height - 780, 'water_drop');
					let scaleWaterDrop = 0.1
					let angleWaterDrop = 30
					waterDrop.setScale(scaleWaterDrop).setScrollFactor(0)
					waterDrop.setAngle(angleWaterDrop)
					tweens.add({
					targets: waterDrop,
					alpha: { from: 0, to: 1 },
					duration: 500,
					ease: 'Phaser.Easing.Linear.None',
					delay: 0,
					yoyo: true,
					hold: 1000,
					repeat: 0,
					angle: 45,
					scale: 0.25,
					onComplete: function () {
					
					onEventButton = false;
					var waterOverflowCheck = waterFilled + waterTank
					if(waterOverflowCheck > WATER_GOAL)
					{
						waterTank = waterForLevelUp
						waterForNextLevel = waterOverflowCheck - WATER_GOAL
					}
					waterFilled += waterTank;
					waterForLevelUp = WATER_GOAL - waterFilled
					console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
					textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)


					// kirim ke be status terakhir jumlah tetes
					let stepWidth = LENGTH_MASK_VALUE * (waterTank/WATER_GOAL);
					console.log(`stepWidth = ${stepWidth}`)

					// moving the mask
					tweens.add({
						targets: energyMask,
						duration: 500,
						delay: 0,
						x :`+=${stepWidth}`,
						onComplete: function () {
						console.log(`this.energyMask.x = ${energyMask.x}`)
						waterTank = 0
					 	timeLimit =  TIME_LIMIT
						 timeLeft = TIME_LEFT
						if(energyMask.x >= FULL_ENERGY_MASK_VALUE){
							setTimeout(function(){ 
								// minta be untuk info water goal, level berapa, dan gambar tanaman untuk next level
								// sesuaikan INIT_MASK_VALUE dan LENGTH_MASK_VALUE
								energyMask.x = INIT_MASK_VALUE
								waterFilled = 0
								waterForLevelUp = WATER_GOAL - waterFilled
								console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
								textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

								var stepWidth;
																   
								stepTreePlant++
								treePlant.visible = false
								if(stepTreePlant==1){
									console.log('masuk tahap 1')
									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 320, 'Lumbung Tanam_6');
									let scaleTreePlant = 1.5
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									} else {
										wateringTimer.paused = false

									}
								}else if(stepTreePlant==2){
									console.log('masuk tahap 2')

									treePlant =	addObject.image(cameras.main.width / 2, cameras.main.height - 340, 'Lumbung Tanam_5');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								} else if(stepTreePlant==3){
									console.log('masuk tahap 3')

									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 410, 'Lumbung Tanam_4');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								} else if(stepTreePlant==4){
									console.log('masuk tahap 4')

									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 500, 'Lumbung Tanam_3');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								} else if(stepTreePlant==5){
									console.log('masuk tahap 5')

									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 600, 'Lumbung Tanam_2');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								} else if(stepTreePlant==6){
									console.log('masuk tahap 6')

									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 720, 'Lumbung Tanam_1');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								}else {
									console.log('masuk tahap 0')

									stepTreePlant = 0
									treePlant = addObject.image(cameras.main.width / 2, cameras.main.height - 200, 'Lumbung Tanam_soil');
									let scaleTreePlant = 0.8
									treePlant.setScale(scaleTreePlant).setScrollFactor(0)
									treePlant.visible = true
									if(waterForNextLevel > 0){
										waterFilled += waterForNextLevel
										waterForLevelUp = WATER_GOAL - waterFilled
										console.log(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)
										textWaterForLevelUp.setText(`${waterForLevelUp} tetes lagi untuk ke tahapan selanjutnya`)

										stepWidth = LENGTH_MASK_VALUE * (waterForNextLevel /WATER_GOAL);
										tweens.add({
											targets: energyMask,
											duration: 500,
											delay: 0,
											x :`+=${stepWidth}`,
											onComplete: function () {
												wateringTimer.paused = false
											}
										})
									}else {
										wateringTimer.paused = false

									}

								} 
								waterForNextLevel = 0

												
							}, 500);
                    
                		} else {
							wateringTimer.paused = false

						}
						}
						});
				// energyMask.x += stepWidth;
					
					
					}
					});
				
				
				
			}

    	});
	

	}

	getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	update ()
	{
		

	}
	
	/* START-USER-CODE */

	// Write your code here.

	/* END-USER-CODE */
}

/* END OF COMPILED CODE */

// You can write more code here
