import {
    Scene
} from 'phaser'
import DropsCount from '../sprites/DropsCount'


export default class HomeScene extends Scene {
    constructor() {
        super({
            key: 'HomeScene'
        })
    }

    preload() {
        // load bg image
        this.load.image('background', '../assets/LumbungTanam_PNG/LumbungTanam_BG.png')
        this.load.image('wateringCanBtn', '../assets/wateringCanBtn.svg')
        this.load.image('lumbungHouseBtn', '../assets/LumbungHouse.svg')
        this.load.image("DropsCountImg", "../assets/TetesAir.svg")
    }


    create() {
        // add bg image
        let bgImg = this.add.image((this.cameras.main.width / 2), this.cameras.main.height / 2, 'background')
        let scaleX = this.cameras.main.width / bgImg.width
        let scaleY = this.cameras.main.height / bgImg.height
        let scale = Math.max(scaleX, scaleY)
        bgImg.setScale(scale).setScrollFactor(0)

        // add watering can button
        let wateringCanButton = this.add.image((this.cameras.main.width / 2) + 400, this.cameras.main.height - 250, 'wateringCanBtn').setInteractive({
            useHandCursor: true
        })
        let scaleWateringButton = 3
        wateringCanButton.setScale(scaleWateringButton).setScrollFactor(0)

        // add lumbung house
        let lumbungHouse = this.add.image((this.cameras.main.width / 2) - 400, this.cameras.main.height - 230, 'lumbungHouseBtn').setInteractive({
            useHandCursor: true
        })
        let scaleLumbungHouse = 3
        lumbungHouse.setScale(scaleLumbungHouse).setScrollFactor(0)

        // add drops count image
        let imgDropsCount = this.add.image((this.cameras.main.width / 2) - 400, this.cameras.main.height - 230, "DropsCountImg")
        let scaleImg = 3
        imgDropsCount.setScale(scaleImg).setScrollFactor(0)

        this.scene.add()

    }

    update() {}
}